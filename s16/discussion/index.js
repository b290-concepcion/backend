/*console.log("Hello");*/

// [ S E C T I O N ] ARITHMETIC OPERATORS
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = y - x;
	console.log("Result of subtraction operator: " + difference);

		let product = y * x;
	console.log("Result of product operator: " + product);

	let quotient = y % x;
	console.log("Result of division operator: " + quotient);

	// The assignment operator assigns the value of the **right** operand to a variable.
	let assignmentnumber = 8;
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentnumber = assignmentnumber + 2;
	console.log("Result of addition operator " + assignmentnumber);

	// Shorthand for assignmentnumber = assignmentnumber + 2
	assignmentnumber += 2;
	console.log("Result of addition operator " + assignmentnumber);

	assignmentnumber -= 2;
	console.log("Result of subtraction operator " + assignmentnumber);

	assignmentnumber *= 2;
	console.log("Result of product operator " + assignmentnumber);

	assignmentnumber /= 2;
	console.log("Result of division operator " + assignmentnumber);

	assignmentnumber %= 2;
	console.log("Result of modulo operator " + assignmentnumber);

	/*

	- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6

	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation " + mdas);

	let pemdas = 1 + (2 - 3) * (4 / 5);

	/*
	            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
	            - The operations were done in the following order:
	                1. 4 / 5 = 0.8
	                2. 2 - 3 = -1
	                3. -1 * 0.8 = -0.8
	                4. 1 + -.08 = .2
	 */
		console.log("Result of pemdas operation " + pemdas);

		pemdas = (1 + (2 - 3)) * (4 / 5);
		console.log("Result of pemdas operation " + pemdas);

		// Increment and Decrement

		// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
		let z = 1;

		// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

		let increment = ++z;
		console.log("Result of pre-increment " + increment);
		// The value of "z" was also increased even though we didn't implicitly specify any value reassignment

		console.log("Result of pre-increment " + z);

		// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one

		increment = z++;
		console.log("Result of post-increment " + increment);
		// The value of "z" is at 2 before it was incremented

		console.log("Result of post-increment " + z);

		let decrement = --z;
		console.log("Result of pre-decrement " + increment);
		console.log("Result of pre-decrement " + z);

		decrement = z--;
		console.log("Result of post-decrement " + increment);
		console.log("Result of post-decrement " + z);

		// [ S E C T I O N ] COMPARISON OPERATORS
		let juan = "juan";
			// (Loose) Equality Operator
		console.log(1==1);
		console.log(1==2);
		console.log(1=="1");
		console.log(0==false);
		console.log("juan" == "juan");
		console.log("juan" == juan);

		// Strict Equality Operator
		/* 
		            - Checks whether the operands are equal/have the same content
		            - Also COMPARES the data types of 2 values
		            - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
		            - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
		            - Some programming languages require the developers to explicitly define the data type stored in variables to prevent this from happening
		            - Strict equality operators are better to use in most cases to ensure that data types provided are correct
		 */

		console.log(1===1);
		console.log(1===2);
		console.log(1==="1");
		console.log(0===false);
		console.log("juan" === "juan");
		console.log("juan" === juan);

		//(loose) inequality operator


			/* 
			            - Checks whether the operands are not equal/have different content
			            - Attempts to CONVERT AND COMPARE operands of different data types
			        */

		console.log(1!=1);
		console.log(1!=2);
		console.log(1!="1");
		console.log(0!=false);
		console.log("juan" != "juan");
		console.log("juan" != juan);

		// Strict Inequality Operator

		/* 
		            - Checks whether the operands are not equal/have the same content
		            - Also COMPARES the data types of 2 values
		        */

		console.log(1!==1);
		console.log(1!==2);
		console.log(1!=="1");
		console.log(0!==false);
		console.log("juan" !== "juan");
		console.log("juan" !== juan);

		//Some comparison operators check whether one value is greater or less than to the other value.

		let a=50;
		let b=65;

		let isgreaterthan=a>b;
		let islessthan=a<b;
		console.log(isgreaterthan, islessthan);

		// greater than or equal

		let isgte=a>=b;
		let islte=a<=b;
		console.log(isgte, islte);

		let numstr = '30';
		console.log(a > numstr);

		let str="twenty";
		console.log(b<=str);


		// [ S E C T I O N ]	Logical operators

		let islegalage = true;
		let isregistered = false;

		//AND operator (&& - Double Ampersand)

		let allrequirementsmet = islegalage && isregistered;
		console.log("Result of logical AND operator " + allrequirementsmet);

		// OR operator (|| = double pipe/bar)
		allrequirementsmet = islegalage||isregistered;
		console.log("Result of logical OR operator " + allrequirementsmet);

		// NOT Operator (! exclamation point)

		let somerequiremenysnotmet =!isregistered;

		console.log("Result of logical NOT operator " + somerequiremenysnotmet);





