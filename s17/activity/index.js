console.log("Hello World");

/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.

*/

	



function getUserInfo(){

	let varA = {
		name: "Nigel",
	age: 32,
	address: "123 mayhugot manila",
	isMarried: false,
	petName: "Momo",
	};
return varA;


};
getUserInfo();

let UserInfo=getUserInfo();
console.log(UserInfo)


/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

function getArtistsArray() {
	let varB = ["Ne-yo", "Linkin Park", "Parokya ni Edgar", "Eminem", "Francis Magalona"];
return varB;
}
let ArtistsArray=getArtistsArray();
console.log(ArtistsArray);


/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getSongsArray() {
	let varB = ["Never Knew I needed", "Numb", "Your Song", "Rap God", "Kaleidoscope World"];
return varB;
}
let SongsArray=getSongsArray();
console.log(SongsArray);


/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getMoviesArray() {
	let varC = ["White Chicks", "21 Jump street", "Limitless", "Avengers: Infinity War", "Avengers: End Game"];
return varC;
}
let MoviesArray=getMoviesArray();
console.log(MoviesArray);


/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

	function getPrimeNumberArray() {
	let varC = ["23", "71", "59", "47", "5"];
return varC;
}
let PrimeNumberArray=getPrimeNumberArray();
console.log(PrimeNumberArray);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}