const http=require("http");

//creates variable "port" to store the port number
const port = 4000;

//craetes a variable "app" that stores the output of "createServer()" method

const app=http.createServer((req,res)=>{

	if(req.url=="/greeting"){	res.writeHead(200, {"Content-Type":"text/plain"});

	//Send the response with text content "Hello World!"
	res.end("Hello World!")}
	else if(req.url=="/homepage"){
			res.writeHead(200, {"Content-Type":"text/plain"});

	//Send the response with text content "This is the homepage"
	res.end("This is the homepage")
	}
	else{	res.writeHead(404, {"Content-Type":"text/plain"});

	//Send the response with text content "404: Page not found"
	res.end("404: Page not found")}
});

//Uses the "app" and "port" variables created above
app.listen(port);
console.log(`Server now running at localhost: ${port}.`);


