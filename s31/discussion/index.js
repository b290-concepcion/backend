//Use the "require" directive to load Node.js
//A "Package" or "module" is a software component or part of a program that contains one or more routine/methods/functions
//The "http module" lets node.js transfer data using hyper text transfer protocol
// clients (browser) and servers (node/express) communicate by exchanging individual messages

let http=require("http");

//using this module "createServer()" method, we can create an http server

//A port is a virtual point where network connection start and end.
//Each port is associated with specific process or service

http.createServer(function (request, response){
	//writeHead() method
	//Set the status code for the response - 200 ->OK/SUCCESS

	response.writeHead(200, {"Content-Type":"text/plain"});

	//Send the response with text content "Hello World!"
	response.end("Hello World!")


}).listen(4000);

//whenever the server starts, console will print the message in our "terminal"
console.log("Server running at localhost:4000");











