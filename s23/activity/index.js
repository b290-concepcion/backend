// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainer={
	name: "Nigel Concepcion",
	age: 28,
	pokemon: ["Frosslass", "Heracross", "Arcanine", "Gardevoir" ],
	friends: {
		Hoenn: ["May", "Max"],
		Kanto: ["Brock", "Misty"],
	 
	},
	talk: function(){
		return "Pikachu! I choose you!";
	}


}
console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
console.log(trainer.talk());

function Pokemon(name,level){
	//properties
	this.name=name;
	this.level=level;
	this.health=2*level;
	this.attack=level;

	//methods

	
	this.tackle = function(target) {

	    console.log( this.name + " tackled " + target.name);

	    // Reduces the target object's health property by subtracting and reassigning it's value based on the pokemon's attack
	    
		target.health=target.health-this.attack;
		if (target.health<=0){
			
	        target.faint()

			
			
	    } else {
	    	return target.name +"'s health is now reduced to " + target.health
	    }

	};

	// Method is invoked in the tackle method
	this.faint = function(){
	    console.log( this.name +" fainted");
	    return this.name +" fainted"
	
	
}}





	let heracross= new Pokemon("Heracross",33);
	console.log(heracross);
	let frosslass= new Pokemon("Frosslass",40);
	let arcanine= new Pokemon("Arcanine",36);
	let gardevoir= new Pokemon("Gardevoir",42);
	let pidgey= new Pokemon("Pidgey",5);
		console.log(pidgey);
	let caterpie= new Pokemon("Caterpie",4);
		console.log(caterpie);

console.log(caterpie.tackle(heracross));

console.log(heracross.tackle(pidgey));

console.log(pidgey);

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}


