// console.log("Magkaka-label ka na")

// [S E C T I O N] Objects

/*
		    - An object is a data type that is used to represent real world objects
		    - It is a collection of related data and/or functionalities
		    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
		    - Information stored in objects are represented in a "key:value" pair
		    - A "key" is also mostly referred to as a "property" of an object
		    - Different data types may be stored in an object's property creating complex data structures
		*/

		// Creating objects using object initializers/literal notation
		/*
		    - This creates/declares an object and also initializes/assigns it's properties upon creation
		    - A cellphone is an example of a real world object
		    - It has it's own properties such as name, color, weight, unit model and a lot of other things
		    - Syntax
		        let objectName = {
		            keyA: valueA,
		            keyB: valueB
		        }
		*/


let cellphone ={
	name: "Nokia 3210",
	manufactureDate: 1999

};
console.log("Result for creating objects using initializers/literal notation: ");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using CONSTRUCTOR FUNCTIONS
    /*
		    - Creates a reusable function to create several objects that have the same data structure
		    - This is useful for creating multiple instances/copies of an object
		    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
		    - Syntax
		        function ObjectName(keyA, keyB) {
		            this.keyA = ValueA;
		            this.keyB = ValueB;
		        }
		*/
    
    // This is an object
		// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
    
    function Laptop(name, manufactureDate){
    	this.name=name;
    	this.manufactureDate=manufactureDate;
    };


// This is a unique instance of the Laptop object
		/*
		    - The "new" operator creates an instance of an object
		    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
		*/

    let laptop = new Laptop("Lenovo",2008);
console.log("Result for creating objects using object constructor: ");
console.log(laptop);    


let myLaptop = new Laptop("MacBook Air",2020);
console.log("Result for creating objects using object constructor: ");
console.log(myLaptop); 

// Creating EMPTY OBJECTS

let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);


// [S E C T I O N] Accessing Object properties
// Using the dot notation
console.log("Result for the dot notation: " +myLaptop.name);

//Using square bracket notation
console.log("Result for the dot notation: " +myLaptop["name"]);

/*
		    - Accessing array elements can be also be done using square brackets
		    - Accessing object properties using the square bracket notation and array indexes can cause confusion
		    - By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
		    - Object properties have names that makes it easier to associate pieces of information
		*/

//Accessing array objects

let array = [laptop, myLaptop];
// May cause confusion for accessing array indexes
console.log(array[0]["name"]);
// Differentiation between accessing arrays and object properties
console.log(array[0].name);
// This tells us that array[0] is an object by using the dot notation



//[ S E C T I O N] initializing/adding/deleting/reassigning object properties

/*
		    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
		    - This is useful for times when an object's properties are undetermined at the time of creating them
		*/    

let car ={};

car.name="Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);


// Initializing/adding object properties using bracket notation
		/*
		    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		    - This also makes names of object properties to not follow commonly used naming conventions for them
		*/
    
car["manufacture date"]=2019;
console.log(car["manufacture date"]);
console.log("Result from adding properties using square bracket: ");
console.log(car);

// Deleting object properties

delete car["manufacture date"]
console.log("Result from using delete: ");
console.log(car);

//Reassigning object properties
car.name="Volkswagen Beetle";
console.log("Result from reassigning properties: ");
console.log(car);

function persons(name,age,personality,device){
this.name=name;
this.age=age;
this.personality=personality;
this.device=device;
}

let personA=new persons("Nigel",28,"sad boi","Desktop")
console.log(personA); 

let personB=new persons("Eula",28,"masaya","Laptop")
console.log(personB); 

//[ S E C T I O N] object method
/*
		    - A method is a function which is a property of an object
		    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
		    - Methods are useful for creating object specific functions which are used to perform tasks on them
		    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
		*/    
    
let person ={
	name: "Juan",
	talk: function(){
		console.log("Kumusta? Ako si "+this.name);
	}
}
console.log(person)
console.log("Result for the object methods: ");
person.talk();
person.walk=function(){
	console.log(this.name+" walked 25 steps forward.");
}
person.walk();

let friend = {
	firstName: "Pedro",
	lastName: "Penduko",
	address: {
		city: "Manila",
		country: "Philippines"
	},
	emails: ["pedro@mail.com", "penduko101@mail.com"],
	introduce: function(){
		console.log("Hello! My name is "+this.firstName+" "+this.lastName)
	}
}
friend.introduce();

//[ S E C T I O N] Real-world applications of objects
  /*
		    - Scenario
		        1. We would like to create a game that would have several pokemon interact with each other
		        2. Every pokemon would have the same set of stats, properties and functions
		*/


let myPokemon = {
	name: "Heracross",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(this.name+" Used tackle against target Pokemon")
	},
	faint: function(){
		console.log(this.name+" fainted")
	}
}

console.log(myPokemon);

//Creating an object constructor instead will healp with this process
function Pokemon(name,level){
	//properties
	this.name=name;
	this.level=level;
	this.health=2*level;
	this.attack=level;

	//methods

	this.tackle=function(target){
		console.log(this.name+" tackled "+target.name);
		console.log(target.name+"'s health is now reduced to _targetPokemonHealth_");
	}
};

// Creates new instances of the "Pokemon" object each with their unique properties

let Heracross= new Pokemon("Heracross",16);
let Frosslass= new Pokemon("Frosslass",48);
Frosslass.tackle(Heracross);