// console.log("yawa")

/*function printInput(){
	let nickname = prompt("Enter your nickname: ");
	return "Hola " + nickname;

}
printInput();*/

function printName(name){
	return "My name is " + name;
}

// parameters with the prompt don't need hardcoded arguments since the values will be provided via prompt


console.log(printName("Yawa"));


let petname = "Hammond";


function printPetName(petName){
	return "My name is " + petName;
}

console.log(printPetName(petname));

/*function printInput(nickname){
	nickname = prompt("Enter your nickname: ");
	return "Hola " + nickname;

}
console.log(printInput());*/

function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " +num +"divided by 8 is" +remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is "+num+ "isDivisibleBy8?");
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*Function parameters can also accept other functions as arguments
Some complex functions use other functions as arguments to perform more complicated results.
This will be further seen when we discuss array methods.*/

function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed");
}

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);
console.log(argumentFunction);

/*Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.
*/

function createFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " "+ lastName;
}

console.log(createFullName("Juan","Dela", "Cruz"));

