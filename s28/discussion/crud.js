//CRUD Operations

//[SECTION] Inserting documents
//Syntax: db.collectionName.insertOne({object})

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "09123456789",
		email: "janedoe@gmail.com"
	},
	courses ["CSS", "Javascript", "Phyton"],
	department: "none"
});

// Inserting multiple documents
// Syntax: db.collectionName.insertMany([{objectA}, {objectb}]);
db.users.insertMany([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "09123456789",
		email: "stephenhawking@mail.com"

	},
	courses: ["Phyton", "React", "PHP"],
	department: "none"
},
{

	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "09123456789",
		email: "neilarmstrong@mail.com"

	},
	courses: ["React", "Laravel", "Sass"],
	department: "none"
}
])

//	[SECTION] FINDING documents (Read)
/* Syntax: 
	db.collectionName.find();
	db.collectionName.findOne();


*/
//Leaving the search criteria empty will retrieve all the documents
db.users.find({firstName: "Stephen"});

// Finding documents with multiple parameters
//Syntax: db.collectionName.find({fieldA: valueA, fieldB: valueB})
db.users.find({lastName: "Armstrong",age: 83})

// [SECTION] UPDATING DOCUMENTS (Update)
//Updating a single document
//SYNTAX: db.collectionName.updateOne({criteria}, {$set: (field: value)});
/*
	updateOne will only update the fiest document that matches the search criteria
*/

//document to be updated


db.users.insertOne({	
	firstName: "test",
	lastName: "test",
	age: 0,
	contact: {
		phone: "0000000000",
		email: "test@mail.com"

	},
	courses: [],
	department: "none"

})

db.users.updateOne(
{firstName: "test"},
{
	$set : {
	firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact: {
		phone: "09123456789",
		email: "bill@mail.com"

	},
	courses: ["PHP", "Laravel", "HTML"],
	department: "Operations",
	status: "active"





			}			

		}

	)

db.users.find({firstName : "Bill"})

//Updating Multiple Documents

/*
	Syntax:
	db.collectionName.updateMany({criteria}, {$set: {field:value}})
*/


db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	}
	)

//Replace One
//Can be used if replacing the whole document is needed
db.users.replaceOne(
	{firstName: "Bill"},
	{
			firstName: "Bill",
	lastName: "Gates",
	age: 65,
	contact: {
		phone: "09123456789",
		email: "bill@mail.com"

	},
	courses: ["PHP", "Laravel", "HTML"],
	department: "Operations",
	status: "active"
})

//[SECTION] Deleting documents (Delete)

// Create document to be deleted
db.users.insertOne({
	firstName: "test"
})


//Deleting a single document
/*
	Syntax: 
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "test"});

//Deleting many
// 	db.collectionName.deleteMany({criteria})


db.users.deleteMany({firstName: "Bill"})


//[section] advance queries

db.users.find({
	contact: {
		phone: "09123456789",
		email: "stephenhawking@mail.com"
	}
})

db.users.find({"contact.email":"janedoe@gmail.com"})

// Querying an array with exact elements (will search elements in order)
db.users.find({courses: ["CSS","Javascript", "Python"})

//search elements in any order

	db.users.find({courses: {all: ["React", "Python"]}})



//Querying an embedded array
db.users.insertOne({
	namearr:[
	{
		namea: "juan"
	},
	{
		nameb: "tamad"
	}]
})


db.users.find({
	namearr: {
		namea: "juan"
	}
})


