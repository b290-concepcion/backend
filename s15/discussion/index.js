//JavaScript renders web pages in an interactive and dynamic fashion. Meaning, it enables you to create dynamically updating content, control multimedia, animate images
		//Let get started by introducing the basic syntax elements of JavaScript.


		// [SECTION] Syntax, Statements and Comments

		
		//Statements:

		// Statements in programming are instructions that we tell the computer to perform
		// JS statements usually end with semicolon (;)
		// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends
		// A syntax in programming, it is the set of rules that describes how statements must be constructed
		// All lines/blocks of code should be written in a specific manner to work. This is due to how these codes were initially programmed to function and perform in a certain manner

		// Ctrl + / = single line comment
		//ctrl +shift + / = multi line comment

   		 /*
		Where To Place JavaScript
			- Inline You can place JavaScript right into the HTML page using the script tags. This is good for very small sites and testing only. The inline approach does not scale well, leads to poor organization, and code duplication.
			-	External File A better approach is to place JavaScript into separate files and link to them from the HTML page. This way a single script can be included across thousands of HTML pages, and you only have one place to edit your JavaScript code. This approach is also much easier to maintain, write, and debug.
		
		Use of the Script Tag
			In the past, we had to worry about specifying many attributes for the script tag. 

		Where should I place the Script Tags?
			The script tags can go anywhere on the page, but as a best practice, many developers will place it just before the closing body tag on the HTML page. This provides faster speed load times for your web page.

		*/

console.log("Hi");


// [SECTION] Variables

		// It is used to contain data.
		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information


		// Declaring variables:

		// Declaring variables - tells our devices that a variable name is created and is ready to store data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

		/* syntax: 
		let Variable name 
		const variablename = value
		*/

let firstvariable;

		// console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console
		// Constant use of this throughout developing an application will save us time and builds good habit in always checking for the output of our code

console.log(firstvariable);


// [SECTION] Variables

		// It is used to contain data.
		// Any information that is used by an application is stored in what we call a "memory"
		// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
		// This makes it easier for us associate information stored in our devices to actual "names" about information


		// Declaring variables:

		// Declaring variables - tells our devices that a variable name is created and is ready to store data
		// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".
    

// Variables must be declared first before they are used
		// Using variables before they're declared will return an error


// Trying to print out a value of a variable that has not been declared will return an error of "not defined"
		// The "not defined" error in the console refers to the variable not being created/defined, whereas in the previous example, the code refers to the "value" of the variable as not defined.
		// let hello;





// console.log(hello);		

		// Variables must be declared first before they are used
		// Using variables before they're declared will return an error

// let hello;

/*
		    Guides in writing variables:
		        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
		        2. Variable names should start with a lowercase character, use camelCase for multiple words.
		        3. For constant variables, use the 'const' keyword.
		        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.

		    Best practices in naming variables:

				1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

					let firstName = "Michael"; - good variable name
					let pokemon = 25000; - bad variable name

				2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

					let FirstName = "Michael"; - bad variable name
					let firstName = "Michael"; - good variable name

				3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

					let first name = "Mike";

				camelCase is when we have first word in small caps and the next word added without space but is capitalized:

					lastName emailAddress mobileNumber

				Underscores sample:

				let product_description = "lorem ipsum"
				let product_id = "250000ea1000"

		*/
		// Declaring and initializing variables
		// Initializing variables - the instance when a variable is given it's initial/starting value

		/* 
		Syntax: 
			let/const variablename = value
		*/

let productname = "desktop computer";
console.log(productname);

let productprice = 18999;
console.log(productprice);

		// In the context of certain applications, some variables/information are constant and should not be changed
		// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
		// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

		const interest = 3.539;

		// Reassigning variable values
		// Reassigning a variable means changing it's initial or previous value into another value

		/* 
			Syntax: variablename = newvalue;

		*/
		productname = "Laptop";
		console.log(productname);

	/*	interest = 3.5;
		console.log(interest);
*/
		let friend = "Lilo";
		friend = "Stitch";
		console.log(friend);

/*		let friend = "Dory"; Uncaught SyntaxError: Identifier 'friend' has already been declared */


		let anime;
		anime = "Neon Genesis Evangelion"; // Initialization, as long as it is the first time that a value is assigned to a variable
		anime = "Gundam Seed";


/*		const pi; Uncaught SyntaxError: Missing initializer in const declaration (at index.js:57:7)
*/	

		//let/const local/global scope
				//Scope essentially means where these variables are available for use
				//let and const are block scoped
				//A block is a chunk of code bounded by {}. A block lives in curly braces. Anything within curly braces is a block.
				//So a variable declared in a block with let  is only available for use within that block.

/*let outervar = "hello";
{
	let innervar = "hello again";
}		

console.log(outervar);
console.log(innervar); //Uncaught ReferenceError: innervar is not defined*/
/*

	if variable is not defined:
		1. Wrong spelling of variable
		2. Casing (JS is Case Sensitive)
		3. Not in scope
		4. Not declared at all
*/

let pokemon = "Pikachu", trainer = "Ash Ketchum";
console.log(pokemon, trainer);
 
    // [SECTION] Data Types

		// Strings
		// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
		// Strings in JavaScript can be written using either a single (') or double (") quote
		// In other programming languages, only the double quotes can be used for creating strings
    	let country = "Philippines";
    	let province = "Bulacan";

    	let fulladdress = province + " , " + country;
    	console.log(fulladdress);

    	let greeting = "I live in "+ province;
    	console.log(greeting);

    	// Concatenating strings
    	// Multiple string values can be combined to create a single string using the "+" symbol

    	// The escape character (\) in strings in combination with other characters can produce different effects

    	let mailaddress = "Bulacan\n\n\n\nPhilippines";
    	console.log(mailaddress);

    	let message = "I'm groot.";
    	console.log(message);
    	message = 'I\'m groot.';
    	console.log(message);

    	// N U M B E R

    	// Integer/whole numbers
    	let headcount = 26;
    	console.log(headcount);

    	// Decimal/Fractions
    	let heatindex = 40.1;
    	console.log(heatindex);

    	// Exponential Notation / Euler's number;
    	let planetdistance = 2e10;
    	console.log(planetdistance);

    	console.log("The heat index today is" + heatindex);

    	// B O O L E A N
    	// Boolean values are normally used to store values relating to the state of certain things
    	// This will be useful in further discussions about creating logic to make our application respond to certain scenarios

    	let ismarried = false;
    	let ingoodconduct = true;
    	console.log("ismarried " + ismarried);
    	console.log("ingoodconduct " + ingoodconduct);

    	//  A R R A Y S
    	// Arrays are a special kind of data type that's used to store multiple values
    	// Arrays can store different data types but is normally used to store similar data types

    	/*

		Syntax:
			let/const arrayname = [elementA, elementB, elementC, ...]

    	*/

    	let grades = [98.7, 92.1, 90.5, 94.6];
    	console.log(grades);

    	// different data types
    	// storing different data types inside an array is not recommended because it will not make sense to in the context of programming

    	let details = ["John", "Smith", 32, false];
    	console.log(details);

    	// O B J E C T S

    	// Objects are another special kind of data type that's used to mimic real world objects/items
    	// They're used to create complex data that contains pieces of information that are relevant to each other
    	// Every individual piece of information is called a property of the object

    	/*
			Syntax:
				let/const objectname = {
					propertyA : value;
					propertyb : value;


				};
		    	*/

    	let person = {
    		fullname : "Juan Dela Cruz",
    		age : 35,
    		ismarried : false,
    		contact : ["+639 123 4567 890"],

    		address : {
    			housenumber : "456",
    			city : "Manila",


    		}
    	};
    	console.log(person);

    	let mygrades = {
    		firstgrading : 98.7,
    		secondgrading: 92.1,
    		thirdgrading: 90.5,
    		fourthgrading: 94.6


    	}
		console.log(mygrades);

		//typeof operator is used to determine the type of data or the value of a variable. It outputs a string.
		console.log(typeof mygrades);
		console.log(typeof grades);

		    /*
				Constant Objects and Arrays
					The keyword const is a little misleading.

					It does not define a constant value. It defines a constant reference to a value.

					Because of this you can NOT:

					Reassign a constant value
					Reassign a constant array
					Reassign a constant object

					But you CAN:

					Change the elements of constant array
					Change the properties of constant object

				*/

		const animes = ["K-on", "Yakitate Japan", "Cyber Kuro-chan"];
		animes[2] = "Kimetsu no yaiba";

		//We can change the element of an array assigned to a constant variable.
		//We can also change the object's properties assigned to a constant variable.

		console.log(animes)

		// N U L L
		// It is used to intentionally express the absence of a value in a variable declaration/initialization
		// null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified
		
		let spouse = null;
		let mynumber = 0;
		let mystring = "";

		// U N D E F I N E D
		// Represents the state of a variable that has been declared but without an assigned value
		let feelings;
		console.log(feelings);

		// Undefined vs Null

		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
		// null means that a variable was created and was assigned a value that does not hold any value/amount
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing
		let varA = null;
		console.log(varA);
	


