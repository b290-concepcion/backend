const express=require("express");
const router = express.Router();
const auth=require("../auth");
const userController=require("../controllers/userController.js")
// Route for checking if the user's email already exists in the database
		// Invokes the checkEmailExists function from the controller file to communicate with our database
		// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
});

//Router for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
})

router.post("/details",(req,res)=>{
	userController.getProfile(req.body).then(resultFromController=>res.send(resultFromController))
})

//route to enroll user to a course

router.post("/enroll",(req,res)=>{
	const isLoggedIn = req.headers.authorization;

	if(isLoggedIn!=undefined&&isLoggedIn!=null){

	let data={
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController=>res.send(resultFromController))
	} else {
		console.log({ auth : "unauthorized user"})
		res.send(false);
	}
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports=router;



























