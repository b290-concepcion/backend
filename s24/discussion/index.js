// [S E C T I O N] EXPONENT OPERATOR
const firstNum=8**2; //ES6 Update
console.log(firstNum);

const secondNum=Math.pow(8,2);
console.log(secondNum);

// [S E C T I O N] Template literals
/*
			- Allows to write strings without using the concatenation operator (+)
			- Greatly helps with code readability
		*/

let name = "John";

// Pre-template literal String

// Uses single/double quotes (''/"")
let message="Hello "+name+"! Welcome to programming!";
console.log("Message without template literals: "+message);

// Strings using template literals

//Use backsticks (``)

message =`Hello ${name}! Welcome to programming!`;
console.log("Message with template literals: "+message);

//Multi-line using template literals
const anotherMessage=`
${name} attended a math competition.
He won it by solving the problem 8**2 with the solution of ${firstNum}.
`;
console.log(anotherMessage);

/*
			- Template literals allow us to write strings with embedded JavaScript expressions
			- expressions are any valid unit of code that resolves to a value
			- "${}" are used to include JavaScript expressions in strings using template literals
		*/

const interestRate =0.1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal*interestRate}`)

// [S E C T I O N] ARRAY DESTRUCTURING

/*
			- Allows to unpack elements in arrays into distinct variables
			- Allows us to name array elements with variables instead of using index numbers
			- Helps with code readability
			- Syntax
				let/const [variableName, variableName, variableName] = array;
		*/

const fullName=["Juan","Dela","Cruz"];

//pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`);

//Array destructuring
const [firstName, middleName,lastName]=fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

// [S E C T I O N] Object DESTRUCTURING

/*
			- Allows to unpack properties of objects into distinct variables
			- Shortens the syntax for accessing properties from objects
			- Syntax
				let/const {propertyName, propertyName, propertyName} = object;
		*/

const person={
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz",
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again!`);

//Object Destructuring
const {givenName, maidenName, familyName}=person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again!`);

function getFullName({givenName,maidenName,lastName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

//[S E C T I O N] ARROW FUNCTIONS

/*
			- Compact alternative syntax to traditional functions
			- Useful for code snippets where creating functions will not be reused in any other portion of the code
			- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
		*/

		/*
	Syntax:
		const variableName = () => {
			console.log()
		}
		*/

	const hello=()=>{
		console.log("Hello World");
	}

// Pre-Arrow function and Template Literals

/*	function printFullName (firstName,midleInitial,lastName){
		console.log(firstName+" "+middleName+ " "+lastName);
	}
*/
	//Arrow Function

/*
			- Syntax
				let/const variableName = (parameterA, parameterB, parameterC) => {
					console.log();
				}
		*/

const printFullName=(firstName, middleInitial,lastName)=> {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}
printFullName("Joh","D", "Smith");

const students=["Owen", "Moon","Gracia"];
//Arrow Functions with loops
//pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student`)
});
// The function is only used in the "forEach" method to print out a text with the student's names

students.forEach((student)=>{
	console.log(`${student} is a student`)
})

//[S E C T I O N] IMPLICIT RETURN STATEMENT

/*
			- There are instances when you can omit the "return" statement
			- This works because even without the "return" statement JavaScript implicitly adds it for the result of the function
		*/
	

//Arrow function in a code block

/*const add=(x,y)=>{
	return x+y;
}
let total=add(1,2);
console.log(total);*/

//Arrow function in one-line
const add=(x,y)=>x+y;

let total=add(1,2);
console.log(total);

//[S E C T I O N ] DEFAULT FUNCTION ARGUMENT VALUE

const greet = (name="User")=> {
	return `Good morning, ${name}!`;
}

console.log(greet("Nezuko"));

//[S E C T I O N] CLASS-BASED OBJECT BLUEPRINTS

/*
			- Allows creation/instantiation of objects using classes as blueprints
		*/


/*
			- The constructor is a special method of a class for creating/initializing an object for that class.
			- The "this" keyword refers to the properties of an object created/initialized from the class
			- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
- Syntax
				class className {
					constructor(objectPropertyA, objectPropertyB) {
						this.objectPropertyA = objectPropertyA;
						this.objectPropertyB = objectPropertyB;
					}
				}
		*/

class Car {
	constructor(brand,name,year){
		this.brand=brand;
		this.name=name;
		this.year=year;
	}
}

//Instantiating an object

/*
			- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
			- No arguments provided will create an object without any values assigned to it's properties
			- let/const variableName = new ClassName();
		*/



const myCar= new Car();


/*
			- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
			- It does not mean that it's properties cannot be changed/immutable
		*/
		

console.log(myCar);

myCar.brand="Ford";
myCar.name="Ranger Raptor";
myCar.year="2021";

console.log(myCar);

// Creating/instantiating a new object from the car class with initialized values

const myNewCar= new Car("Toyota","Vios",2021);

console.log(myNewCar);



